package com.alphabyte.physics.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.content.Context;
import android.content.ClipboardManager;
import android.content.ClipData;
import android.view.View;

import com.alphabyte.physics.R;
import com.alphabyte.physics.model.Equation;
import com.alphabyte.physics.model.PhysicsEquation;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.kexanie.library.MathView;

public class DetailActivity extends AppCompatActivity {

    String lineBreak =  "\\(\\\\\\)";
    String topic;
    PhysicsEquation equation;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.fab) FloatingActionButton fab;
    @BindView(R.id.formula_box) MathView formulaView;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        topic = bundle.getString("name");
        equation = (PhysicsEquation) bundle.getSerializable("data");


        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(topic);
        }

        formulaView.config(
                "MathJax.Hub.Config({\n"+
                "  CommonHTML: { linebreaks: { automatic: true } },\n"+
                "  \"HTML-CSS\": { linebreaks: { automatic: true } },\n"+
                "         SVG: { linebreaks: { automatic: true } }\n"+
            "});");


        final String data = getData(equation);
        formulaView.setText(data);
        final String formulaViewText = formulaView.getText();


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE); 
                ClipData clip = ClipData.newPlainText("label", formulaViewText);
                clipboard.setPrimaryClip(clip);
                Snackbar.make(view,"Copied Data To Clipboard",Snackbar.LENGTH_LONG).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

   
    public String getData(PhysicsEquation equation){
        StringBuilder stringBuilder = new StringBuilder("Equations :- \n");
        stringBuilder.append(lineBreak);
        List<Equation> list = equation.getEquations();
        for(int i = 0 ; i  <  list.size(); i++){
            Equation e = list.get(i);
            stringBuilder.append(e.getEquation());
            //stringBuilder.append(lineBreak);
        }

        stringBuilder.append("Information :-");
        stringBuilder.append(lineBreak);


        List<String> information = equation.getInformation();
        //stringBuilder.append("\\begin{itemize}");
        for(int i = 0 ; i < information.size(); i++){
            //stringBuilder.append("\\(\\item " + information.get(i));
            stringBuilder.append(information.get(i));
            stringBuilder.append(lineBreak);
            //stringBuilder.append("\\(\\\\\\)");
            //stringBuilder.append("\\");
        }
        //stringBuilder.append("\\(\\end{itemize}\\)");


        return stringBuilder.toString();
    }
}
