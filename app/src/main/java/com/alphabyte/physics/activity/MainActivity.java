package com.alphabyte.physics.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.alphabyte.physics.R;
import com.alphabyte.physics.adapter.HomeAdapter;
import com.alphabyte.physics.adapter.ListAdapter;
import com.alphabyte.physics.model.PhysicsEquation;
import com.alphabyte.physics.helper.DividerItemDecoration;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements ListAdapter.AdapterListener {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.fab) FloatingActionButton fab;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    //HomeAdapter adapter;
    ListAdapter adapter;
    Gson gson;
    List<PhysicsEquation> physicsEquationList;

    private ActionModeCallback actionModeCallback;
    private ActionMode actionMode;
    public static String PACKAGE_NAME;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        
        PACKAGE_NAME = getApplicationContext().getPackageName();

        setSupportActionBar(toolbar);
        gson = new Gson();
        physicsEquationList = getEquation();
        adapter = new ListAdapter(physicsEquationList,MainActivity.this,this);
        //adapter = new HomeAdapter(physicsEquationList,MainActivity.this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapter);

        actionModeCallback = new ActionModeCallback();


        /*
        recyclerView.addOnItemTouchListener(new ListAdapter.RecyclerTouchListener(MainActivity.this, recyclerView, new HomeAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                PhysicsEquation equation = physicsEquationList.get(position);
                Intent intent = new Intent(MainActivity.this,DetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("name",equation.getName());
                bundle.putInt("position",position);
                bundle.putSerializable("data",equation);
                intent.putExtras(bundle);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        })); 

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }); */
    }


    public List<PhysicsEquation> getEquation(){
        List<PhysicsEquation> physicsEquation;
        String data = null;
        Type equationList = new TypeToken<ArrayList<PhysicsEquation>>(){}.getType();
        
        try {
            data = AssetJSONFile("data.json", MainActivity.this);
        }catch (IOException e) {
            e.printStackTrace();
        }

        if(data != null){
            physicsEquation = gson.fromJson(data, equationList);
        }else{
            physicsEquation = new ArrayList<>();
            PhysicsEquation error = new PhysicsEquation();
            error.setName("Error");
            error.setInformation(Arrays.asList("Error Getting Data","Possible Failure"));
            error.setEquations(null);
        }

        return physicsEquation;
    }

    public static String AssetJSONFile (String filename, Context context) throws IOException {
        AssetManager manager = context.getAssets();
        InputStream file = manager.open(filename);
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();
        return new String(formArray);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onIconClicked(int position) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(actionModeCallback);
        }           
 
        toggleSelection(position);
    }

    @Override
    public void onListItemClicked(int position) {
        // verify whether action mode is enabled or not
        // if enabled, change the row state to activated
        if (adapter.getSelectedItemCount() > 0) {
            enableActionMode(position);
        } else {
            // Open Details Activity 
            PhysicsEquation equation = physicsEquationList.get(position);
            Intent intent = new Intent(MainActivity.this,DetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("name",equation.getName());
            bundle.putInt("position",position);
            bundle.putSerializable("data",equation);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    @Override
    public void onRowLongClicked(int position) {
        // long press is performed, enable action mode
        enableActionMode(position);
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }
 
    private void toggleSelection(int position) {
        adapter.toggleSelection(position);
        int count = adapter.getSelectedItemCount();
 
        if (count == 0) {
            actionMode.finish();
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }

    }

    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_action_mode, menu);

            return true;
        }
 
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }
 
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_favourite:
                    favourite();
                    mode.finish();
                    return true;
 
                default:
                    return false;
            }
        }
 
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            adapter.clearSelections();
            actionMode = null;
            
            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    adapter.resetAnimationIndex();
                    // mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    private void favourite(){
        // Does Nothing Now 
        // Add Item to Realm Db or SQlite
    }
 
}
