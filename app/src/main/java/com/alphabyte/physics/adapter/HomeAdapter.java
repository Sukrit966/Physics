package com.alphabyte.physics.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alphabyte.physics.R;
import com.alphabyte.physics.model.PhysicsEquation;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 *
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder>{

    List<PhysicsEquation> equationList;
    Context context;

    public HomeAdapter(List<PhysicsEquation> equationList, Context context) {
        this.equationList = equationList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        ViewHolder viewHolder;
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_equation, parent, false);
        viewHolder = new ViewHolder(view,viewType);
        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PhysicsEquation equation = equationList.get(position);
        holder.title.setText(equation.getName());

	String image_url = getUrl(equation.getName());
	
	// Loads Appropriate Image For The Topic 
        Glide.with(context)
                .load(image_url)
                .into(holder.thumbnail);
    }

    public String getUrl(String topic){
    	// Temporary Placeholder Image is Loaded 
    	return "http://i.polywallpapers.tk/1.png";
    }

    @Override
    public int getItemCount() {
        return equationList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public ImageView thumbnail;
        public CardView cardView;

        public ViewHolder(View v, int viewType) {
            super(v);
            cardView = (CardView) v.findViewById(R.id.cardView);
            title = (TextView) v.findViewById(R.id.topic);
            thumbnail = (ImageView) v.findViewById(R.id.topic_image);
        }
    }


    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private HomeAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final HomeAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }


}
