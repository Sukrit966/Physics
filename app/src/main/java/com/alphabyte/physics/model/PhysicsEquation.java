package com.alphabyte.physics.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PhysicsEquation implements Serializable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("equations")
    @Expose
    private List<Equation> equations = null;
    @SerializedName("information")
    @Expose
    private List<String> information = null;

    public PhysicsEquation() {
    }

    public PhysicsEquation(String name, List<Equation> equations, List<String> information) {
        this.name = name;
        this.equations = equations;
        this.information = information;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Equation> getEquations() {
        return equations;
    }

    public void setEquations(List<Equation> equations) {
        this.equations = equations;
    }

    public List<String> getInformation() {
        return information;
    }

    public void setInformation(List<String> information) {
        this.information = information;
    }

}
