
package com.alphabyte.physics.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Equation implements Serializable{

    @SerializedName("equation")
    @Expose
    private String equation;

    public Equation() {
    }

    public Equation(String equation) {
        this.equation = equation;
    }

    public String getEquation() {
        return equation;
    }

    public void setEquation(String equation) {
        this.equation = equation;
    }

}
